<?php

use Tygh\Registry;
use Tygh\Languages\Languages;
use Tygh\BlockManager\Block;
use Tygh\Navigation\LastView;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_get_videos($params = array(), $items_per_page = 0, $lang_code = CART_LANGUAGE, $showAll = true)
{
    $params = LastView::instance()->update('video_manager', $params);

    $default_params = array(
        'page' => 1,
        'items_per_page' => $items_per_page
    );

    $params = array_merge($default_params, $params);

    $sortings = array(
        'name' => '?:video_descriptions.name',
        'status' => '?:videos.status',
    );

    $condition = $limit = '';

    if (!empty($params['limit'])) {
        $limit = db_quote(' LIMIT 0, ?i', $params['limit']);
    } elseif (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(DISTINCT(?:videos.video_id)) FROM ?:videos WHERE 1 ?p", $conditions);
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $sorting = db_sort($params, $sortings, 'name', 'asc');

    $condition .= fn_get_localizations_condition('?:videos.localization');

    if (!empty($params['item_ids'])) {
        $condition .= db_quote(' AND ?:videos.video_id IN (?n)', explode(',', $params['item_ids']));
    }

    $condition .= (AREA == 'A') ? '' : " AND ?:videos.status IN ('A') ";

    fn_set_hook('get_videos', $params, $condition, $sorting, $limit, $lang_code);

    $fields = array (
        '?:videos.video_id',
        '?:videos.status',
        '?:video_descriptions.name',
        '?:video_descriptions.url',
    );

    $videos = db_get_hash_array(
        "SELECT ?p FROM ?:videos " .
        "LEFT JOIN ?:video_descriptions ON ?:video_descriptions.video_id = ?:videos.video_id " .
        "WHERE ?:video_descriptions.lang_code = ?s ?p ?p ?p",
        'video_id', implode(", ", $fields), $lang_code, $condition, $sorting, $limit
    );

    $result = array();
    foreach($videos as $video) {
        $v = $video;
        $v['code'] = convertToEmbedded($video['url']);
        $result[] = $v;
    }

    fn_set_hook('get_videos_post', $videos, $params);

    return array($result, $params);
}

function fn_get_videos_except($product_id = 0, $lang_code = CART_LANGUAGE)
{
    $condition = '';
    $condition .= fn_get_localizations_condition('?:videos.localization');
    $condition .= (AREA == 'A') ? '' : " AND ?:videos.status IN ('A', 'H') ";
    $fields = array (
        '?:video_descriptions.video_id',
        '?:video_descriptions.name',
        '?:video_descriptions.url',
    );
    $videos = db_get_hash_array(
        "SELECT ?p FROM ?:videos " .
        "LEFT JOIN ?:video_descriptions ON ?:video_descriptions.video_id = ?:videos.video_id " .
        "WHERE ?:video_descriptions.lang_code = ?s " .
	        "AND ?:video_descriptions.video_id NOT IN (SELECT ?:video_links.video_id FROM ?:video_links WHERE ?:video_links.product_id = ?p) ?p",
            'video_id', implode(", ", $fields), $lang_code, $product_id, $condition
    );
    return array($videos);
}

function fn_get_videos_by_product($product_id = 0, $lang_code = CART_LANGUAGE)
{
    $condition = '';
    $condition .= fn_get_localizations_condition('?:videos.localization');

    fn_set_hook('get_videos_by_product', $params, $condition, $sorting, $limit, $lang_code);

    $fields = array (
        '?:video_descriptions.video_id',
        '?:video_descriptions.name',
        '?:video_descriptions.url',
        '?:videos.status',
    );

    $condition .= (AREA == 'A') ? '' : " AND ?:videos.status IN ('A') ";

    $videos = db_get_hash_array(
        "SELECT ?p FROM ?:video_descriptions " .
        "LEFT JOIN ?:video_links ON ?:video_links.video_id = ?:video_descriptions.video_id " .
        "LEFT JOIN ?:videos ON ?:videos.video_id = ?:video_links.video_id " .
        "WHERE ?:video_descriptions.lang_code = ?s AND ?:video_links.product_id = ?p ?p",
        'video_id', implode(", ", $fields), $lang_code, $product_id, $condition
    );
    
    $result = array();
    
    foreach($videos as $video) {
        $v = $video;
        $v['code'] = convertToEmbedded($video['url']);
        $result[] = $v;
    }
    
    fn_set_hook('get_videos_by_product_post', $videos, $params);

    return array($result);
}

function convertToEmbedded($string) {
    $matches = array();
    $result = '';

    preg_match(
        "/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/",
        $string,
        $matches);

    if (strpos($matches[3], 'youtu') !== false) {
        $result = '//www.youtube.com/embed/' . $matches[6];
    } else if (strpos($matches[3], 'vimeo') !== false) {
        $result = '//player.vimeo.com/video/' . $matches[6];
    }

    return $result;
}

function fn_get_video_data($video_id, $lang_code = CART_LANGUAGE)
{
    $fields = $joins = array();
    $condition = '';

    $fields = array (
        '?:videos.video_id',
        '?:videos.status',
        '?:videos.localization',
        '?:videos.timestamp',
        '?:video_descriptions.name',
        '?:video_descriptions.url',
    );

    $joins[] = db_quote("LEFT JOIN ?:video_descriptions ON ?:video_descriptions.video_id = ?:videos.video_id AND ?:video_descriptions.lang_code = ?s", $lang_code);

    $condition = db_quote("WHERE ?:videos.video_id = ?i", $video_id);
    $condition .= (AREA == 'A') ? '' : " AND ?:videos.status IN ('A', 'H') ";

    fn_set_hook('get_video_data', $video_id, $lang_code, $fields, $joins, $condition);

    $video = db_get_row("SELECT " . implode(", ", $fields) . " FROM ?:videos " . implode(" ", $joins) ." $condition");

    if (!empty($video)) {
        //$video['main_pair'] = fn_get_image_pairs($banner['banner_image_id'], 'promo', 'M', true, false, $lang_code);
    }

    fn_set_hook('get_video_data_post', $video_id, $lang_code, $video);

    return $video;
}


function fn_videos_update_video($data, $video_id, $lang_code = DESCR_SL)
{
    if (isset($data['timestamp'])) {
        $data['timestamp'] = fn_parse_date($data['timestamp']);
    }

    $data['localization'] = empty($data['localization']) ? '' : fn_implode_localizations($data['localization']);

    if (!empty($video_id)) {
        db_query("UPDATE ?:videos SET ?u WHERE video_id = ?i", $data, $video_id);
        db_query("UPDATE ?:video_descriptions SET ?u WHERE video_id = ?i AND lang_code = ?s", $data, $video_id, $lang_code);
    } else {
        $video_id = $data['video_id'] = db_query("REPLACE INTO ?:videos ?e", $data);

        foreach (Languages::getAll() as $data['lang_code'] => $v) {
            db_query("REPLACE INTO ?:video_descriptions ?e", $data);
        }
    }

    return $video_id;
}

function fn_delete_video_by_id($video_id)
{
    if (!empty($video_id)) {
        db_query("DELETE FROM ?:videos WHERE video_id = ?i", $video_id);
        db_query("DELETE FROM ?:video_descriptions WHERE video_id = ?i", $video_id);

        fn_set_hook('delete_videos', $video_id);

        Block::instance()->removeDynamicObjectData('videos', $video_id);
    }
}

function fn_add_video_link($video_id, $product_id)
{
    if(!empty($video_id) && !empty($product_id)) {
        $data = array();
        $data['video_id'] = $video_id;
        $data['product_id'] = $product_id;
        
        db_query("REPLACE INTO ?:video_links ?e", $data);
        
        fn_set_hook('add_video_links', $video_id, $product_id);
    }
}

function fn_delete_video_link($video_id, $product_id)
{
    if(!empty($video_id) && !empty($product_id)) {
        db_query("DELETE FROM ?:video_links WHERE video_id = ?i AND product_id = ?i", $video_id, $product_id);
        
        fn_set_hook('delete_video_links', $video_id, $product_id);

        Block::instance()->removeDynamicObjectData('vides', $video_id);
    }
}
