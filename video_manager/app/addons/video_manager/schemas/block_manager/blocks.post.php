<?php

$schema['main']['cache_overrides_by_dispatch']['products.view']['update_handlers'][] = 'videos';
$schema['main']['cache_overrides_by_dispatch']['products.view']['update_handlers'][] = 'video_descriptions';

$schema['video_manager'] = array(
    'templates' => array (
        'addons/video_manager/blocks/product_tabs/video_manager.tpl' => array(),
    )
);

return $schema;