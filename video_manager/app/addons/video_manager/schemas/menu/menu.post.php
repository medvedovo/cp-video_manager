<?php

$schema['central']['website']['items']['video_manager'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'href' => 'video_manager.manage',
    'position' => 500
);

return $schema;
