<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'view') {
    $product = Tygh::$app['view']->getTemplateVars('product');
    if(!empty($product['product_id'])) {
        list($videos,) = fn_get_videos_by_product($product['product_id']);
    }

    Tygh::$app['view']->assign('videos', $videos);
}
