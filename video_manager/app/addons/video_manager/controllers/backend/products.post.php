<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    return;
}

if ($mode == 'update') {
    Registry::set('navigation.tabs.video_manager', array (
        'title' => __('video_tab'),
        'js' => true
    ));

    $product = Tygh::$app['view']->getTemplateVars('product_data');

    if (!empty($product['product_id'])) {
        list($videos,) = fn_get_videos_by_product($product['product_id'], DESCR_SL, false);
        Tygh::$app['view']->assign('product_id', $product['product_id']);
    }
    Tygh::$app['view']->assign('videos', $videos);
}
