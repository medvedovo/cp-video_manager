<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']	== 'POST') {
    fn_trusted_vars('videos', 'video_data');
    $redirect = 'video_manager';

    if ($mode == 'm_delete') {
        foreach ($_REQUEST['video_ids'] as $v) {
            fn_delete_video_by_id($v);
        }
        $redirect .= '.manage';
    }

    if ($mode == 'update') {
        $video_id = fn_videos_update_video($_REQUEST['video_data'], $_REQUEST['video_id']);
        $redirect .= ".update?video_id=$video_id";
    }

    if ($mode == 'delete') {
        if (!empty($_REQUEST['video_id'])) {
            fn_delete_video_by_id($_REQUEST['video_id']);
        }
        $redirect .= '.manage';
    }

    if($mode == 'add_link') {
        $product_id = $_REQUEST['product_id'];
        fn_add_video_link($_REQUEST['video_id'], $product_id);
        
        $redirect = "products.update&product_id=$product_id"; 
    }

    if($mode == 'remove_link') {
        $product_id = $_REQUEST['product_id'];
        fn_delete_video_link($_REQUEST['video_id'], $product_id);
        
        $redirect = "products.update&product_id=$product_id"; 
    }

    return array(CONTROLLER_STATUS_OK, $redirect);
}

if ($mode == 'update') {
    $video = fn_get_video_data($_REQUEST['video_id']);

    if (empty($video)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    Registry::set('navigation.tabs', array (
        'general' => array (
            'title' => __('general'),
            'js' => true
        ),
    ));

    Tygh::$app['view']->assign('video', $video);
} elseif ($mode == 'manage') {
    $params = $_REQUEST;
    list($videos, $search) = fn_get_videos($params, Registry::get('settings.Appearance.admin_elements_per_page'));

    Tygh::$app['view']->assign('videos', $videos);
    Tygh::$app['view']->assign('search', $search);
}

if ($mode == 'picker') {
    if (!empty($_REQUEST['product_id'])) {
        list($videos,) = fn_get_videos_except($_REQUEST['product_id']);
        Tygh::$app['view']->assign('product_id', $_REQUEST['product_id']);
        Tygh::$app['view']->assign('videos', $videos);
    }
    Tygh::$app['view']->display('addons/video_manager/views/video_manager/components/picker.tpl');
    exit;
}
