{capture name="mainbox"}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}

<form action="{""|fn_url}" method="post" name="videos_form" class=" cm-hide-inputs" enctype="multipart/form-data">
{include file="common/pagination.tpl" save_current_page=true save_current_url=true}

{if $videos}
<table class="table table-middle">
<thead>
<tr>
    <th width="1%" class="left">
        {include file="common/check_items.tpl" class="cm-no-hide-input"}</th>
    <th width="50%"><a class="cm-ajax{if $search.sort_by == "name"} sort-link-{$search.sort_order_rev}{/if}" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("name")}</a></th>

    {hook name="videos:manage_header"}
    {/hook}

    <th width="6%">&nbsp;</th>
    <th class="right" width="12%"><a class="cm-ajax{if $search.sort_by == "status"} sort-link-{$search.sort_order_rev}{/if}" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("status")}</a></th>
</tr>
</thead>
{foreach from=$videos item=video}
<tr class="cm-row-status-{$video.status|lower}">
    {assign var="allow_save" value=$video|fn_allow_save_object:"videos"}

    {if $allow_save}
        {assign var="no_hide_input" value="cm-no-hide-input"}
    {else}
        {assign var="no_hide_input" value=""}
    {/if}

    <td class="left">
        <input type="checkbox" name="video_ids[]" value="{$video.video_id}" class="cm-item {$no_hide_input}" /></td>
    <td class="{$no_hide_input}">
        <a class="row-status" href="{"video_manager.update?video_id=`$video.video_id`"|fn_url}">{$video.name}</a>
    </td>

    {hook name="videos:manage_data"}
    {/hook}

    <td>
        {capture name="tools_list"}
            <li>{btn type="list" text=__("edit") href="video_manager.update?video_id=`$video.video_id`"}</li>
        {if $allow_save}
            <li>{btn type="list" class="cm-confirm" text=__("delete") href="video_manager.delete?video_id=`$video.video_id`" method="POST"}</li>
        {/if}
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
    <td class="right">
        {include file="common/select_popup.tpl" id=$video.video_id status=$video.status hidden=true object_id_name="video_id" table="videos" popup_additional_class="`$no_hide_input` dropleft"}
    </td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{capture name="buttons"}
    {capture name="tools_list"}
        {if $videos}
            <li>{btn type="delete_selected" dispatch="dispatch[video_manager.m_delete]" form="videos_form"}</li>
        {/if}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}
{capture name="adv_buttons"}
    {include file="common/tools.tpl" tool_href="video_manager.add" prefix="top" hide_tools="true" title=__("add_video") icon="icon-plus"}
{/capture}

{include file="common/pagination.tpl"}

</form>

{/capture}
{include file="common/mainbox.tpl" title=__("videos") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons select_languages=true}

{** ad section **}