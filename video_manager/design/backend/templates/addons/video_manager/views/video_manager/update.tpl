{if $video}
    {assign var="id" value=$video.video_id}
{else}
    {assign var="id" value=0}
{/if}

{assign var="allow_save" value=$video|fn_allow_save_object:"videos"}

{** videos section **}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" class="form-horizontal form-edit  {if !$allow_save} cm-hide-inputs{/if}" name="videos_form" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
<input type="hidden" class="cm-no-hide-input" name="video_id" value="{$id}" />

{capture name="tabsbox"}
<div id="content_general">
    {hook name="videos:general_content"}
    <div class="control-group">
        <label for="elm_video_name" class="control-label cm-required">{__("name")}</label>
        <div class="controls">
        <input type="text" name="video_data[name]" id="elm_video_name" value="{$video.name}" size="25" class="input-large" /></div>
    </div>

    <div class="control-group">
        <label for="elm_video_url" class="control-label cm-required">{__("url")}</label>
        <div class="controls">
        <input type="text" name="video_data[url]" id="elm_video_url" value="{$video.url}" size="25" class="input-large" /></div>
    </div>

    <div class="control-group">
        <label class="control-label" for="elm_video_timestamp_{$id}">{__("creation_date")}</label>
        <div class="controls">
        {include file="common/calendar.tpl" date_id="elm_video_timestamp_`$id`" date_name="video_data[timestamp]" date_val=$video.timestamp|default:$smarty.const.TIME start_year=$settings.Company.company_start_year}
        </div>
    </div>

    {include file="views/localizations/components/select.tpl" data_name="video_data[localization]" data_from=$video.localization}

    {include file="common/select_status.tpl" input_name="video_data[status]" id="elm_video_status" obj_id=$id obj=$video hidden=true}
    {/hook}
</div>
{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}

{capture name="buttons"}
    {if !$id}
        {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="videos_form" but_name="dispatch[video_manager.update]"}
    {else}
        {if "ULTIMATE"|fn_allowed_for && !$allow_save}
            {assign var="hide_first_button" value=true}
            {assign var="hide_second_button" value=true}
        {/if}
        {include file="buttons/save_cancel.tpl" but_name="dispatch[video_manager.update]" but_role="submit-link" but_target_form="videos_form" hide_first_button=$hide_first_button hide_second_button=$hide_second_button save=$id}
    {/if}
{/capture}
    
</form>

{/capture}

{notes}
    {hook name="videos:update_notes"}
    {__("banner_details_notes", ["[layouts_href]" => fn_url('block_manager.manage')])}
    {/hook}
{/notes}

{if !$id}
    {assign var="title" value=__("video_manager.new_video")}
{else}
    {assign var="title" value="{__("video_manager.editing_video")}: `$banner.banner`"}
{/if}
{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons select_languages=true}

{** video section **}
