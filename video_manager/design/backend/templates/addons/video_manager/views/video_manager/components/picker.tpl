{if $videos}
    <table class="table table-middle">
    <thead>
    <tr>
        <th width="50%">{__("name")}</th>
        <th>{__("url")}</th>
        <th>&nbsp;</th>
    </tr>
    </thead>
    {foreach from=$videos item=video}
    <tr class="cm-row-status-{$video.status|lower}">
        <td class="{$no_hide_input}">{$video.name}</td>
        <td>
            <a href="{$video.url}" target="_blank">{$video.url}</a>
        </td>
        <td class="right">
            <a class="btn cm-post cm-confirm" href="{"video_manager.add_link?video_id={$video.video_id}&product_id={$product_id}"|fn_url}">
                <i class="icon-plus"></i>
            </a>
        </td>
    </tr>
    {/foreach}
    </table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}