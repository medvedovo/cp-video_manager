<div id="content_video_manager" class="hidden">
	<div class="right">
		<a class="btn cm-dialog-opener cm-dialog-switch-avail cm-tooltip" href="{"video_manager.picker?product_id={$product_id}"|fn_url}" data-ca-dialog-title="{__("add_video_link")}">
			<i class="icon-plus"></i>
		</a>
	</div>
	<form action="{""|fn_url}" method="post" name="videos_form" class="cm-hide-inputs" enctype="multipart/form-data">

	{if $videos}
		<table class="table table-middle">
		<thead>
		<tr>
			<th width="50%">{__("name")}</th>
			<th>{__("url")}</th>
		</tr>
		</thead>
		{foreach from=$videos item=video}
		<tr class="cm-row-status-{$video.status|lower}">
			<td class="{$no_hide_input}">{$video.name}</td>
			<td>
				<a href="{$video.url}" target="_blank">{$video.url}</a>
			</td>
			<td class="right">
				<a class="btn btn-danger cm-post cm-confirm" href="{"video_manager.remove_link?video_id={$video.video_id}&product_id={$product_id}"|fn_url}">
					<i class="icon-trash icon-white"></i>
				</a>
			</td>
		</tr>
		{/foreach}
		</table>
	{else}
		<p class="no-items">{__("no_data")}</p>
	{/if}

	</form>
</div>