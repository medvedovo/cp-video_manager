{** block-description:videos **}

{if $videos}
    <div class="video_manager" id="content_video_manager">
    {foreach from=$videos item="video"}
        <h3>{$video.name}</h3>
        <div style="position:relative;width:100%;height:0;padding-bottom:56.25%">
            <iframe style="position:absolute;top:0;left:0;width:100%;height:100%" frameborder="0" src="{$video.code}" allowfullscreen></iframe>
        </div>
    {/foreach}
    </div>
{/if}